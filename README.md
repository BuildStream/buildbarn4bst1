
# BuildBarn for BuildStream 1

This repository contains a very simple buildbarn setup to use as an artifact cache for BuildStream 1

This is not a production ready setup and exists solely as a reference for using the traefik
load balancer to properly delegate the asset cache gRPC calls to the bb-asset-cache and to delegate
the storate related gRPC calls to the bb-storage service.

## How to use is

To run this reference artifact cache, just run the following:

```
docker-compose -f docker-compose.yml up
```

And this should setup a cache server which you can use to upload and download artifacts, with
no TLS setup.

Artifacts can be pulled from `localhost:5890` and artifacts can be pushed to `localhost:5891`.


## Why ?

Historically BuildStream 1 used to have it's own artifact cache implementation which supported
all artifact caching operations to be handled over a single URL. With BuildStream 2 client cache
configuration we have the distinction between the `index` service and the `storage` service, and
so a simpler buildbarn setup is possible, there is an [example](https://github.com/apache/buildstream/blob/master/.github/compose/ci.buildbarn.yml)
used in BuildStream 2 tests which can be referenced for this.

This is simply a reference of how to use buildbarn as an artifact cache with BuildStream 1.
